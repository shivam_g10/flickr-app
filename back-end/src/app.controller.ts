import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/:tag')
  getTag( @Param('tag') tag: string): Promise<any>
  {
    return this.appService.getTags(tag);
  }
}
