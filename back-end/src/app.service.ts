import { Injectable } from '@nestjs/common';
import fetch from 'node-fetch';
const FLICKR_API = 'https://api.flickr.com/services/feeds/photos_public.gne?lang=en-us&format=json&tags=';
@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  async getTags(tag: string): Promise<any>
  {
    console.log(FLICKR_API+tag)
    const data = await fetch(FLICKR_API+tag, {
      method: 'GET',
      headers: {
        'Host': 'api.flickr.com'
      }
    });
    let text = await data.text();
    text = text.replace('jsonFlickrFeed(', '').replace('})', '}');
    return JSON.parse(text);
  }
}
