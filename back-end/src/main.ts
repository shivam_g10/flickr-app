import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { NextFunction, Request, Response } from 'express';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  applyAppOptions(app);
  const options = new DocumentBuilder()
        .setTitle('Konnect Server')
        .setDescription('API playground')
        .setVersion('0.0.1')
        .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('playground', app, document,{swaggerOptions:{docExpansion: 'none'}});
  await app.listen(3000);
}


function applyAppOptions(app: INestApplication) 
{
    const cors2 = function(req: Request, res: Response, next: NextFunction) 
    {
        const origin = req.headers.origin;
        res.setHeader('Access-Control-Allow-Credentials','true');
        if(origin)
        {
            res.setHeader('X-ORIGIN', origin );
            res.setHeader('Access-Control-Allow-Origin', origin);
        }
        res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
        //intercepts OPTIONS method
        if ('OPTIONS' === req.method) 
        {
            //respond with 200
            res.status(200).send('OK');
        }
        else 
        {
            //move on
            next();
        }
    };
    app.use(cors2);
}
bootstrap();
