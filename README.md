## Flickr Test APP

### Requirements

* [Node JS](https://nodejs.org/en/)

### Setup

```bash
npm run setup
```

### Run

```bash
npm start
```

Client will be available at [`http://127.0.0.1:8080`](http://127.0.0.1:8080)
Backend will be available at [`http://127.0.0.1:3000/playground/#/default`](http://127.0.0.1:3000/playground/#/default)

### Client

* Plain JS 
* Jquery
* Bootstrap
* Native WebComponents
* Webpack build
* Babel

### Backend

* NodeJS
* NestJS
* Proxy for flickr api


