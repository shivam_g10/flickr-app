import { triggerApi } from './helpers';
import { APP_CONSTANTS } from './constants';

/**
 * 
 * @param {string} tag 
 */
export async function getDataFromFlickr(tag)
{
    const url = APP_CONSTANTS.BACKEND_FLICKR_PROXY + tag;
    return await triggerApi(url, 'GET', null, 'application/json');
}