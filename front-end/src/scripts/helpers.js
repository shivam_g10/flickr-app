import { APP_CONSTANTS } from './constants';

/**
 * 
 * @param {string} URL 
 * @param {'PUT' | 'GET' | 'POST' | 'DELETE'} METHOD 
 * @param {*} DATA 
 * @param {'application/json'} CONTENT_TYPE 
 * @returns {Promise}
 */
export function triggerApi(URL, METHOD, DATA, CONTENT_TYPE)
{
    let apiParams = {
        'url': URL,
        'method': METHOD,
        'xhrFields': {
            withCredentials: true 
        },
        'crossDomain': true
    };
    if(METHOD === 'POST' || METHOD === 'PUT' || (METHOD === 'DELETE' && DATA != ''));
    {
        apiParams['contentType'] = 'application/x-www-form-urlencoded';
        apiParams['data'] = DATA;
    }
    if(CONTENT_TYPE)
    {
        apiParams['contentType'] = CONTENT_TYPE;
    }
    return new Promise(function(resolve, reject) 
    {
        apiParams['success'] = function(data) 
        {
            $.unsubscribe(APP_CONSTANTS.EVENTS.API_CANCEL);
            resolve(data); 
        };
        apiParams['error'] = function(data) 
        {
            $.unsubscribe(APP_CONSTANTS.EVENTS.API_CANCEL);
            reject(data);
        };
        const obj = $.ajax(apiParams);
        $.subscribe(APP_CONSTANTS.EVENTS.API_CANCEL, () => 
        {
            obj.abort();
            $.unsubscribe(APP_CONSTANTS.EVENTS.API_CANCEL);
            $.publish(APP_CONSTANTS.EVENTS.API_ABORTED);
        });
    });
}

/**
 * 
 * @param {Date} time 
 * @returns {string}
 */
export function timeSince(date) 
{

    var seconds = Math.floor((new Date() - date) / 1000);
  
    var interval = seconds / 31536000;
  
    if (interval > 1) 
    {
        return Math.floor(interval) + ' years ago';
    }
    interval = seconds / 2592000;
    if (interval > 1) 
    {
        return Math.floor(interval) + ' months ago';
    }
    interval = seconds / 86400;
    if (interval > 1) 
    {
        return Math.floor(interval) + ' days ago';
    }
    interval = seconds / 3600;
    if (interval > 1) 
    {
        return Math.floor(interval) + ' hours ago';
    }
    interval = seconds / 60;
    if (interval > 1) 
    {
        return Math.floor(interval) + ' minutes ago';
    }
    return Math.floor(seconds) + ' seconds ago';
}