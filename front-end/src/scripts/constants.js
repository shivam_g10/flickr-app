export const APP_CONSTANTS  = {
    FLICKR_API: 'https://api.flickr.com/services/feeds/photos_public.gne?lang=en-us&format=json&tags=', // CORS ERROR
    BACKEND_FLICKR_PROXY: 'http://127.0.0.1:3000/',
    EVENTS: {
        TRIGGER_SEARCH: 'trigger-search',
        API_LOADING: 'api-loading',
        API_REQUEST_COMPLETED: 'api-completed',
        API_CANCEL: 'api-cancel',
        API_ABORTED: 'api-aborted'
    }
};