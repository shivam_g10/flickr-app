/**
 * The component to display spinner loading
 * APIs
 * @description data-hidden html attribute can be used to set spinner to hide by default
 * @method show() display loading
 * @method hide() hide loading
 * @method fadeOut() fadeout loading
 * @example <loading-component /> or <loading-component data-hidden="true" />
 */

import './loading-component.css';
const getTemplate = () => 
{
    const template = document.createElement('template');
    template.innerHTML = `
    <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>
    `;
    return template;
};

class LoadingComponent extends HTMLElement
{
    constructor()
    {
        super();
        this.isHidden = false;
    }

    connectedCallback()
    {
        const node = getTemplate().content.cloneNode(true);
        this.appendChild(node);
        this.jqueryEle = $(this.querySelector('.spinner-border'));
        const hidden = this.getAttribute('data-hidden');
        if(hidden && hidden === 'true')
        {
            this.hide();   
        }
    }

    /**
     * Display loading
     */
    show()
    {
        if(this.isHidden)
        {
            this.isHidden = false;
            this.jqueryEle.show();
        }
    }

    /**
     * Hide loading
     */
    hide()
    {
        if(!this.isHidden)
        {
            this.isHidden = true;
            this.jqueryEle.hide();
        }
    }

    /**
     * Fade out loading
     */
    fadeOut()
    {
        if(!this.isHidden)
        {
            this.isHidden = true;
            this.jqueryEle.fadeOut();
        }
    }
}

customElements.define('loading-component', LoadingComponent);
