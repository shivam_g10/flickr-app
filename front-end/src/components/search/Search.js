import './search.css';
import { APP_CONSTANTS } from '../../scripts/constants';
const getTemplate = () =>
{
    const template = document.createElement('template');
    template.innerHTML = `
    <div class="row g-3">
        <div class="col-auto">
            <input type="text" class="form-control-plaintext" name='search' placeholder='E.g. Hawaii'>
        </div>
        <div class="col-auto">
            <button class="btn btn-primary mb-3" id='search-btn'>Search</button>
            <button class="btn btn-secondary mb-3" id='cancel-btn'>Cancel</button>
        </div>
        <div id='common-tags'>
        </div>
    </div>
    `;
    return template;
};
class FlickrSearch extends HTMLElement
{
 
    constructor()
    {
        super();
    }
    connectedCallback()
    {
        $.subscribe(APP_CONSTANTS.EVENTS.API_REQUEST_COMPLETED, this.getCommonTagsHandler());
        $.subscribe(APP_CONSTANTS.EVENTS.API_LOADING, this.getLoadingHandler());
        const node = getTemplate().content.cloneNode(true);
        this.appendChild(node);
        this.querySelector('#search-btn').addEventListener('click', this.getSearchHandler());
        this.querySelector('#cancel-btn').addEventListener('click', this.getCancelHandler());
    }

    getCancelHandler()
    {
        return () =>
        {
            $.publish(APP_CONSTANTS.EVENTS.API_CANCEL);
            this.resetButton();
        };
    }

    resetButton()
    {
        const button = this.querySelector('#search-btn');
        button.disabled = false;
        button.classList.remove('btn-secondary');
        button.classList.add('btn-primary');
    }

    getLoadingHandler()
    {
        return () => 
        {
            const button = this.querySelector('#search-btn');
            button.disabled = true;
            button.classList.remove('btn-primary');
            button.classList.add('btn-secondary');
        };
    }
    getSearchHandler()
    {
        return () => 
        {
            const input = this.querySelector('input');
            const tag = input.value;
            if(!tag)
            {
                input.classList.add('is-invalid');
                return;
            }
            else
            {
                input.classList.remove('is-invalid');
            }
            $.publish(APP_CONSTANTS.EVENTS.TRIGGER_SEARCH, tag);
        };
    }

    getCommonTagsHandler()
    {
        return (event, commonTags) =>
        {
            const button = this.querySelector('#search-btn');
            button.disabled = false;
            button.classList.remove('btn-secondary');
            button.classList.add('btn-primary');
            if(!commonTags)
            {
                return;
            }
            const commonTagsEle = this.querySelector('#common-tags');
            commonTags.innerHTML = '';
            const tags = Object.keys(commonTags);
            let firstTag = {
                tag: tags[0],
                count : commonTags[tags[0]]
            };
            let secondTag = {
                count: 0
            };
            let thirdTag = {
                count: 0
            };
            tags.map((t) =>
            {
                const count = commonTags[t];
                if(count > firstTag.count)
                {
                    thirdTag = {
                        ...secondTag
                    };
                    secondTag = {
                        ...firstTag
                    };
                    firstTag.tag = t,
                    firstTag.count = count;
                }
                else if(count > secondTag.count)
                {
                    thirdTag = {
                        ...secondTag
                    };
                    secondTag.tag = t,
                    secondTag.count = count;
                }
                else if(count > thirdTag.count)
                {
                    thirdTag.tag = t,
                    thirdTag.count = count;
                }
            });
            commonTagsEle.innerHTML = `
            <div>
                <h3>${firstTag.tag}</h3>
            </div>
            Top 3 tags:
                <span class="badge bg-info text-dark" data-tag='${firstTag.tag}'>${firstTag.tag} (${firstTag.count})</span>
                ${
    !secondTag.tag?'':
        `<span class="badge bg-primary" data-tag='${secondTag.tag}'>${secondTag.tag} (${secondTag.count})</span>`
}
                ${
    !thirdTag.tag? '' :
        `<span class="badge bg-success" data-tag='${thirdTag.tag}'>${thirdTag.tag} (${thirdTag.count})</span>`
}
                
            `;
            commonTagsEle.querySelector('.bg-info').addEventListener('click',this.getTriggerBadgeHandler());
            commonTagsEle.querySelector('.bg-primary').addEventListener('click',this.getTriggerBadgeHandler());
            commonTagsEle.querySelector('.bg-success').addEventListener('click',this.getTriggerBadgeHandler());

        };
    }

    getTriggerBadgeHandler()
    {
        return (event) => 
        {
            const tag = event.target.getAttribute('data-tag');
            const input = this.querySelector('input');
            input.value = tag;
            this.getSearchHandler()();
        };
    }
}
 
customElements.define('flickr-search', FlickrSearch);