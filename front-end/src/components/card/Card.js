import './card.css';
import { timeSince } from '../../scripts/helpers';
const getTemplate = (data) =>
{
    const template = document.createElement('template');
    template.innerHTML = `
    <div class="card" style="width: 18rem;">
        <img src="${data.media.m}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">${data.title}</h5>
            <div class='time-since'>
                ${timeSince(new Date(data.published))}
            </div>
        </div>
    </div>
    `;
    return template;
};
class FlickrCard extends HTMLElement
{
 
    constructor()
    {
        super();
    }
    setData(data)
    {
        this.data = data;
    }
    connectedCallback()
    {
        const node = getTemplate(this.data).content.cloneNode(true);
        this.appendChild(node);
    }
}
 
customElements.define('flickr-card', FlickrCard);