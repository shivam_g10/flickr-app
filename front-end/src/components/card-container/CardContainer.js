import './card-container.css';
import { APP_CONSTANTS } from '../../scripts/constants';
import { getDataFromFlickr } from '../../scripts/api';

const getTemplate = () => 
{
    const template = document.createElement('template');
    template.innerHTML = `
    <loading-component data-hidden='true'></loading-component>
    <div class='container'>
        <div class='row' id='card-container'></div>
    </div>
    `;
    return template;
};

class FlickrCardContainer extends HTMLElement
{
 
    constructor()
    {
        super();
    }
    connectedCallback()
    {
        $.subscribe(APP_CONSTANTS.EVENTS.TRIGGER_SEARCH, this.getSearchHandler());
        $.subscribe(APP_CONSTANTS.EVENTS.API_ABORTED, this.getAbortHandler());
        const node = getTemplate().content.cloneNode(true);
        this.appendChild(node);
        this.load = this.querySelector('loading-component');
        this.container = this.querySelector('#card-container');
    }

    getAbortHandler()
    {
        return () =>
        {
            this.load.fadeOut();
        };
    }

    getSearchHandler()
    {
        return async (event, tag) => 
        {
            $.publish(APP_CONSTANTS.EVENTS.API_LOADING);
            this.load.show();
            let data = null;
            try
            {
                data = await getDataFromFlickr(tag);
            }
            catch(e)
            {
                // Error in API
                console.log(e);
                return;
            }
            this.load.fadeOut();
            this.container.innerHTML = '';
            if(data && data.items && data.items.length > 0)
            {
                let publishTimeArray = [];
                // eslint-disable-next-line object-curly-newline
                const commonTags = {};
                // eslint-disable-next-line object-curly-newline
                const images = {};
                data.items.map((item) => 
                {
                    const published = new Date(item.published);
                    const tags = item.tags.split(' ');
                    tags.map((t) => 
                    {
                        if(commonTags[t])
                        {
                            commonTags[t]++;
                        }
                        else
                        {
                            commonTags[t] = 1;
                        }
                    });
                    const milis = published.valueOf();
                    publishTimeArray.push(milis);
                    let imageArray = [];
                    if(images[milis])
                    {
                        imageArray = images[milis];
                    }

                    imageArray.push(item);

                    images[milis] = imageArray;
                });
                publishTimeArray = publishTimeArray.sort().reverse();
                
                publishTimeArray.map((time) =>
                {
                    const imageArray = images[time];
                    imageArray.map((i) => 
                    {
                        const card = document.createElement('flickr-card');
                        card.classList.add('col-sm-3');
                        card.setData(i);
                        this.container.appendChild(card);
                    });
                });
                $.publish(APP_CONSTANTS.EVENTS.API_REQUEST_COMPLETED, commonTags);
            }
            else
            {
                // eslint-disable-next-line object-curly-newline
                const commonTags = {};
                commonTags[tag] = 0;
                $.publish(APP_CONSTANTS.EVENTS.API_REQUEST_COMPLETED, commonTags);
            }
            
        };
    }
}
 
customElements.define('flickr-card-container', FlickrCardContainer);