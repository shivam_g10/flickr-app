/* eslint-disable no-undef */
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = () => 
{
    console.log('Initiating webpack build...');

    return {
        mode: 'development',
        entry: {
            index: './src/index.js',
        },
        devtool: 'hidden-source-map',
        devServer: {
            contentBase: './dist',
            compress: true,
            port: 8080,
            historyApiFallback: {
                index:'/index.html',
                rewrites: [
                    { 
                        from: /.*/, to: '/index.html' 
                    }
                ]
            }
        },
        output: {
            filename: 'js/app.[hash].js',
            chunkFilename: 'js/[name].[hash].bundle.js',
            path: path.resolve(__dirname, 'dist'),
            publicPath: '/'
        },
        plugins: [
            new CompressionPlugin(),
            new CleanWebpackPlugin(),
            new MiniCssExtractPlugin({
                filename: 'css/[name].[hash].css',
            }),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
            }),
            new HtmlWebpackPlugin({
                favicon: './favicon.ico',
                title: 'Flickr',
                template: './src/index.html'
            })
        ],
        optimization: {
            minimizer: [new TerserJSPlugin({
                sourceMap: true,
                terserOptions: {
                    ecma: 5,
                },
                extractComments: 'all'
            }), new OptimizeCSSAssetsPlugin({
            })],
            splitChunks: {
                chunks: 'all',
                cacheGroups: {
                    vendor: {
                        chunks: 'initial',
                        name: 'vendor',
                        test: 'vendor',
                        enforce: true,
                    },
                },
            },
            usedExports: true,
        },
        module: {
            rules: [
                {
                    test: /\.js?$/,
                    include: [
                        path.resolve(__dirname, 'src')
                    ],
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    },
                },
                {
                    test: /\.css$/i,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                        },
                        {
                            loader: 'css-loader',
                        }
                    ],
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    include: [
                        path.resolve(__dirname, 'src/images')
                    ],
                    loader: 'file-loader?name=images/[name].[hash].[ext]'
                },
                {
                    test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                    exclude: [
                        path.resolve(__dirname, 'src/images')
                    ],
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: false,
                                name: '[name].[hash].[ext]',
                                outputPath: 'fonts'
                            },
                        }
                    ]
                }
            ]
        }
    };
};